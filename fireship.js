setInterval(() => {
  document
    .querySelectorAll('[free=""]')
    .forEach((el) => el.setAttribute("free", true));

  if (document.querySelector('if-access [slot="granted"]')) {
    document.querySelector('if-access [slot="denied"]').remove();
    document
      .querySelector('if-access [slot="granted"]')
      .setAttribute("slot", "denied");
  }

  if (
    document.querySelector("video-player")?.shadowRoot?.querySelector(".vid")
      ?.innerHTML
  )
    return;
  const vimeoId = document.querySelector("global-data").vimeo;
  const youtubeId = document.querySelector("global-data").youtube;

  if (vimeoId) {
    document.querySelector("video-player").setAttribute("free", true);
    document
      .querySelector("video-player")
      .shadowRoot.querySelector(
        ".vid"
      ).innerHTML = `<iframe src="https://player.vimeo.com/video/${vimeoId}" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="" title="${
      location.pathname.split("/")[3]
    }" width="426" height="240" frameborder="0"></iframe>`; // set video
  }
  if (youtubeId) {
    document.querySelector("video-player").setAttribute("free", true);
    document
      .querySelector("video-player")
      .shadowRoot.querySelector(
        ".vid"
      ).innerHTML = `<iframe src="https://youtube.com/embed/${youtubeId}" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="" title="${
      location.pathname.split("/")[3]
    }" width="426" height="240" frameborder="0"></iframe>`;
  }
}, 100);
